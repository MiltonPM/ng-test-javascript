exports = (typeof window === 'undefined') ? global : window;

exports.functionsAnswers = {
  argsAsArray : function(fn, arr) {
    
      //Helpers
      fn = function(arr) {
        sayItCalled = true;
        return greeting + ', ' + name + (punctuation || '!');
      };
    

  },

  speak : function(fn, obj) {
    /*
      Helpers 
      var sayIt = function(greeting, name, punctuation) {
        sayItCalled = true;
        return greeting + ', ' + name + (punctuation || '!');
      };   
      var fn = function() {
          return sayIt(this.greeting, this.name, '!!!');
        };
     */
    

  },

  functionFunction : function(str) {

  },

  makeClosures : function(arr, fn) {

  },

  partial : function(fn, str1, str2) {
    /**
     * Helpers
     * fn = function(greeting, name, punctuation) {
        sayItCalled = true;
        return greeting + ', ' + name + (punctuation || '!');
      };
     */
  },

  useArguments : function() {

  },

  callIt : function(fn) {

  },

  partialUsingArguments : function(fn) {
    /*
      Helpers     
      var partialMe = function (x, y, z) {
        return x / y * z;
      };
     */
  },

  curryIt : function(fn) {
    /**
     * Helpers
     * var fn = function (x, y, z) {
        return x / y * z;
      };
     */
  }
};

exports = (typeof window === 'undefined') ? global : window;

exports.arraysAnswers = {

  indexOf : function(arr, item) {
    indexOf = arr.indexOf(item);
    /*for (var index =  0; index < arr.length; index++) {
      if(arr[index] == item)
        indexOf = index;
    }*/
    return indexOf;
  },

  sum : function(arr) {
      var sum = 0;
      for (var index = 0; index < arr.length; index++)
        sum += arr[index];
      return sum;
  },

  remove : function(arr, item) {    
    for(var i = 0;i<arr.length; i++){
      if (arr[i] === item) arr.splice(i, 1);
    }
    return remove = arr; 
  },

  removeWithoutCopy : function(arr, item) {
     
  },

  append : function(arr, item) {
    arr.push(item);    
    return append = arr;
  },

  truncate : function(arr) {
    var truncate = arr.slice(0, (arr.length - 1));
    return truncate;
  },

  prepend : function(arr, item) {
    var prepend = [item];
    prepend = prepend.concat(arr);
    return prepend;
  },

  curtail : function(arr) {
    var curtail = arr.slice(1, arr.length);
    return curtail;
  },

  concat : function(arr1, arr2) {
    var concat = arr1.concat(arr2);
    return concat;
  },

  insert : function(arr, item, index) {
    var insert = arr.slice(0, index);
    insert.push(item);
    insert = insert.concat(arr.slice(index, arr.length));
    return insert;
  },

  count : function(arr, item) {
    var count = 0;
    for (var i=0; i< arr.length; i++){
      if (arr[i] == item) {
        count++;
      }
    }
    return count;
  },

  duplicates : function(arr) {
    var duplicates = [];
    for (var i=0; i< arr.length; i++){

      var count = 0;
      for (var j=0; j < arr.length; j++){
        if (arr[j] == arr[i]) {
          count++;
        }
      }

      if (count > 1) {
        if (duplicates.indexOf(arr[i]) == -1)
          duplicates.push(arr[i]);
      }
    }
    return duplicates;
  },

  square : function(arr) {
      var square = [];
      for (var j=0; j < arr.length; j++){
        square[j] = Math.pow(arr[j], 2);
      }
      return square;
  },

  findAllOccurrences : function(arr, target) {
    findAllOccurrences = [];
    for (var j=0; j < arr.length; j++){
      if(arr[j] == target)
        findAllOccurrences.push(j);    
    }
    return findAllOccurrences;
  }
};

exports = (typeof window === 'undefined') ? global : window;

exports.logicalOperatorsAnswers = {
  or : function(a, b) {
      var or = a || b;
      return or;
  },

  and : function(a, b) {
      var and = a && b;
      return and;
  }
};

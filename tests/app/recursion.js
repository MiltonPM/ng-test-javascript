if ( typeof window === 'undefined' ) {
  require('../../app/recursion');
  var expect = require('chai').expect;
  var _ = require('underscore');
}

describe('permutation', function() {
  var arr = [ 1, 2, 3, 4 ];
  var answer = [
    [1, 2, 3, 4],
    [1, 2, 4, 3],
    [1, 3, 2, 4],
    [1, 3, 4, 2],
    [1, 4, 2, 3],
    [1, 4, 3, 2],
    [2, 1, 3, 4],
    [2, 1, 4, 3],
    [2, 3, 1, 4],
    [2, 3, 4, 1],
    [2, 4, 1, 3],
    [2, 4, 3, 1],
    [3, 1, 2, 4],
    [3, 1, 4, 2],
    [3, 2, 1, 4],
    [3, 2, 4, 1],
    [3, 4, 1, 2],
    [3, 4, 2, 1],
    [4, 1, 2, 3],
    [4, 1, 3, 2],
    [4, 2, 1, 3],
    [4, 2, 3, 1],
    [4, 3, 1, 2],
    [4, 3, 2, 1]
  ];

  it('#permute(arr) you should be able to return the permutations of an array', function() {
    var result = recursionAnswers.permute(arr);
    var resultStrings = _.map(result, function(r) { return r.join(''); });

    expect(result.length).to.eql(answer.length);

    _.each(answer, function(a) {
      expect(resultStrings.indexOf(a.join('')) > -1).to.be.ok;
    });
  });

  it('#fibonacci(number) you should be able to return the nth number in a fibonacci sequence', function() {
    expect(recursionAnswers.fibonacci(2)).to.eql(1);
    expect(recursionAnswers.fibonacci(6)).to.eql(8);
  });

  it('#validParentheses(n) you should be able to return the set of all valid combinations of n pairs of parentheses.', function() {
    var expected = [ '((()))', '(()())', '(())()', '()(())', '()()()'];
    var result = recursionAnswers.validParentheses(3);

    expect(result.length).to.eql(5);
    _.each(expected, function(c) {
      expect(result).to.contain(c);
    });
  });
});
